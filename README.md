# QA

There is 2 classes and 2 macro examples.

"yunoPicoDstQaRunId" - for bad runId rejection, 
it is need to be performed before regular QA.
Vector with bad runs from this class must be inserted in the regular QA.

RunAnalyzerRunId.C - macro for launching "yunoPicoDstQaRunId". 
You need to set vector of triggers and runID range which you want to analyze. 
For each dataset it would be uniq.

"yunoPicoDstQa" - for regular QA.

"RunAnalyzer.C" - macro for launching "yunoPicoDstQa".
Here you need to set vector of bad runs, trigger ids, event, track cuts, number
of pid (and cuts for pid), which you want to use. All setters now have reasonable 
values.

//////////////////////////////////////////////////////
// This class was developed by Eugenia Khyzhniak
// eugenia.sh.el@gmail.com
//////////////////////////////////////////////////////

#ifndef yunoPicoDstQa_hh
#define yunoPicoDstQa_hh

#include <iostream>

#include "TROOT.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TMath.h"
#include "TString.h"
#include "TVector3.h"
#include "TLorentzVector.h"

#define _VANILLA_ROOT_
#include "StPicoEvent/StPicoDstReader.h"
#include "StPicoEvent/StPicoDst.h"
#include "StPicoEvent/StPicoEvent.h"
#include "StPicoEvent/StPicoTrack.h"
#include "StPicoEvent/StPicoBTofPidTraits.h"

using namespace std;

const double MassProton = 0.9382720;
const double MassKaon = 0.4936770;
const double MassPion = 0.139570;
const double MassElectron = 0.5110e-03;

/////////////////////////////////////////////////////
class yunoPicoDstQa {
public :
  yunoPicoDstQa();
  ~yunoPicoDstQa();

  //All business of QA
  void PerformQaRegular(const char *inFile, const char *outFile);

  //File variables
  TFile *mOutFile;
  StPicoDstReader* picoReader;
  StPicoTrack *mPrimTrack;
  StPicoDst *mDst;
  StPicoEvent *mEvent;
  StPicoBTofPidTraits *mTrait;

  // Event variables
  unsigned int mNEvents;
  vector <int> mBadRunId, mTriggerId;
  float mNumberOfPT, mNumberOfGT, mRunId, mRefMult, mTofMult,
    mVertPositionX, mVertPositionY, mVertPositionZ, mVpdVz,
    mZdcE, mZdcW, mRateZDC, mBbcE, mBbcW, mRateBBC;
  int iBeforeAllCut, iRunId, iTriggerId, iRefMultCut, iZVertexCut, iXYZCut, iVzDiffCut, iXYSqrCut;
  float mVzCut[2], mXYZCut,  mVzDiffCut, mXYSqrCut;

  // Track variables
  int mNPrimTracks;
  float mPx, mPy, mPz, mPt, mP, mEta, mPhi, mCharge, mChi2,
    mDeDx, mDeDxSigmaProton, mDeDxSigmaKaon, mDeDxSigmaPion, mDeDxSigmaElectron,
    mDcaX, mDcaY, mDcaZ, mDcaMag, mDcaPerp,
    mNHits, mNHitsFit, mNHitsPoss, mNHitsDedx, mR,
    mTOF, mInvBeta, mBeta, mMassSqr,
    mInvBetaExpectedProton, mInvBetaExpectedKaon, mInvBetaExpectedPion, mInvBetaExpectedElectron;
  int iNhits, iRCut, iMom, iMomPt, iEta, iDca;
  double mNHitsCut[2], mShift[2], mRCut[2], mMomCut[2], mMomPtCut[2], mEtaCut[2], mDcaCut[2];

  // Particle variables
  int mIdentType;
  double mProtonMassSqr[2], mInvBetaProton[2], mProtonMom, mNSigmaProton[5];
  double mKaonMassSqr[2], mInvBetaKaon[2], mKaonMom, mNSigmaKaon[5];
  double mPionMassSqr[2], mInvBetaPion[2], mPionMom, mNSigmaPion[5];
  vector<unsigned int> mProtonPosId, mKaonPosId, mPionPosId;
  vector<unsigned int> mProtonNegId, mKaonNegId, mPionNegId;

  // Histo variables
  TH1D *hNeventsAfterEachCut, *hNtracksAfterEachCut, *hRefMultBeforeRunIdCut;
  struct strHisto1D {const char *name; double nBins, lo, hi;  };
  struct strHisto2D {const char *name; struct opt {double nBins, lo, hi;} x,y;  };
  struct Histogram {
    vector<TH1D*> H1D;
    vector<TH2F*> H2D;
  } mBCVtx, mACVtx, mBCtrc, mACtrc, mProton, mKaon, mPion;

  // Creation and filling histos
  void CreateOutHistogram();
  void CreateVtxHistogram(struct Histogram *hist, const char *text);
  void FillVtxHistogram(struct Histogram *hist);
  void CreateTrackHistogram(struct Histogram *hist, const char *text);
  void FillTrackHistogram(struct Histogram *hist);
  void CreatePidHistogram(struct Histogram *hist, const char *text);
  void FillPidHistogram(unsigned int iGlTr, vector<unsigned int> &PosId, vector<unsigned int> &NegId, struct Histogram *hist);

  // Cleaners
  void EventVariablesClear();
  void TrackVariablesClear();

  // Cuts
  bool CutRunId();
  bool CutTriggerId();
  bool CutEvent();
  bool CutTrack();
  bool CutParticle(double nSigmaPart, double nSigmaOterPart1, double nSigmaOterPart2, double nSigmaOterPart3,
                   double ParticleMom, double nSigmaLimit0, double nSigmaLimit1, double nSigmaLimit2,
                   double nSigmaLimit3, double nSigmaLimit4, double minMassSqr, double maxMassSqr,
                   double invDiff, double minInvBeta, double maxInvBeta);

  // Setters
  void SetBadRunIds(vector<int> runIds = {
      12127007, 12127009, 12127012, 12127014, 12127015,
      12127016, 12127024, 12127046, 12128016, 12128024,
      12128028, 12128031, 12133002, 12133003, 12133004,
      12135027, 12135052, 12136080, 12138081, 12138082,
      12138087, 12138088, 12138089, 12138090, 12138091,
      12138092, 12139002, 12139003, 12139006, 12139007,
      12139008, 12139009, 12139010, 12139015, 12139016,
      12139017, 12139018, 12139019, 12139020, 12139021,
      12140026, 12141014, 12141041, 12142013, 12142028,
      12143024, 12144004, 12144032, 12144050, 12144051,
      12149034, 12150036, 12150047, 12150048, 12150050,
      12150057, 12151001, 12151002, 12151003, 12151004,
      12151005, 12151006, 12151009, 12151010, 12151011,
      12151012, 12151013, 12151016, 12151017, 12151023,
      12151032, 12151033, 12151036, 12151038, 12151039,
      12151040, 12151041, 12151044, 12151045, 12151046,
      12151056, 12151061, 12151062, 12152007, 12155011,
      12155012, 12155013, 12156020, 12158069, 12160025,
      12162005, 12163057, 12164063, 12165010, 12167007,
      12168056, 12168063, 12169063, 12171004  });
  void SetTriggerIds(vector<int> trig = {350003,350013,350023,350033,350043});
  void SetVzCutRange(float lo = -30., float hi = 30.);
  void SetXYZCutRange(float cut = 1.e-5);
  void SetVzDiffCutRange(float cut = 3.);
  void SetShiftXY(float x = 0., float y = 0.);
  void SetXYSqrCutRange(float cut = 2.);
  void SetNHitsCutRange(float lo = 15., float hi = 60.);
  void SetRCutRange(float lo = 0.52, float hi = 4.);
  void SetMomCutRange(float lo = 0.15, float hi = 10.);
  void SetMomPtCutRange(float lo = 0.15, float hi = 10.);
  void SetEtaCutRange(float lo = -1., float hi = 1.);
  void SetDcaCutRange(float lo = 0., float hi = 3.);
  void SetPid(int pid = 3);
  void SetProtonCutRange(float loMassSqr = 0.7, float hiMassSqr = 1.1,
                         float loInvBeta = -0.015, float hiInvBeta = 0.015,
                         float mom = 0.45,
                         double nSigma0 = 2., double nSigma1 = 2.,
                         double nSigma2 = 2., float nSigma3 = 2.);
  void SetKaonCutRange(float loMassSqr = 0.18, float hiMassSqr = 0.32,
                       float loInvBeta = -0.015, float hiInvBeta = 0.015,
                       float mom = 0.45,
                       double nSigma0 = 2., double nSigma1 = 2.,
                       double nSigma2 = 2., float nSigma3 = 2.);
  void SetPionCutRange(float loMassSqr = -0.02, float hiMassSqr = 0.062,
                       float loInvBeta = -0.015, float hiInvBeta = 0.015,
                       float mom = 0.45,
                       double nSigma0 = 2., double nSigma1 = 2.,
                       double nSigma2 = 2., float nSigma3 = 2.);
  ClassDef(yunoPicoDstQa, 1)
};
#endif



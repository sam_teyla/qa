#define yunoPicoDstQaRunId_cxx
#include "yunoPicoDstQaRunId.h"

ClassImp(yunoPicoDstQaRunId)

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::EventVariablesClear() {
  iBeforeAllCut = 0;
  iTriggerId = 0;
  iZVertexCut = 0;
  iXYSqrCut = 0;

  mRunId = -999;
  mRefMult = -999;
  mNumberOfPT = -999;
  mVertPositionZ = -999.;
  mVertR = -999.;
} //EventVariablesClear

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetTriggerIds(vector<int> trig) {
  mTriggerId = trig;
};

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetRunIdRange(int first, int last) {
  mRunIdVal[0] = first; mRunIdVal[1] = last;
  mRunIdBins = mRunIdVal[1] - mRunIdVal[0];
} //SetRunIdRange

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetVzCutRange(double lo, double hi) {
  mVzCut[0] = lo; mVzCut[1] = hi;
} //SetVzCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetShiftXY(double x, double y) {
  mShift[0] = x; mShift[1] = y;
}//SetShiftXY

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetXYSqrCutRange(double cut) {
  mXYSqrCut = cut;
} //SetXYSqrCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetNHitsCutRange(double lo, double hi) {
  mNHitsCut[0] = lo;   mNHitsCut[1] = hi;
} //SetNHitsCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetMomPtCutRange(double lo, double hi) {
  mMomPtCut[0] = lo;  mMomPtCut[1] = hi;
} //SetMomPtCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetEtaCutRange(double lo, double hi) {
  mEtaCut[0] = lo;     mEtaCut[1] = hi;
} //SetEtaCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::SetDcaCutRange(double lo, double hi) {
  mDcaCut[0] = lo;      mDcaCut[1] = hi;
} //SetDcaCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::MeanTrackVariablesClear() {
  mGoodPrimTracks = 0;
} //TrackVariablesClear

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::TrackVariablesClear() {

} //TrackVariablesClear

/////////////////////////////////////////////////////
bool yunoPicoDstQaRunId::CutTriggerId() {
  bool isGoodEvent = true;
  for (auto i = mTriggerId.begin(); i != mTriggerId.end(); i++) {
    if(event->isTrigger(*i)) return isGoodEvent;
  }
  isGoodEvent = false;
  return isGoodEvent;
} //CutTriggerId

/////////////////////////////////////////////////////
yunoPicoDstQaRunId::yunoPicoDstQaRunId(){
  SetTriggerIds();
  SetRunIdRange();
  SetVzCutRange();
  SetXYSqrCutRange();
  SetNHitsCutRange();
  SetMomPtCutRange();
  SetEtaCutRange();
  SetDcaCutRange();
} // yunoPicoDstQaRunId()

/////////////////////////////////////////////////////
yunoPicoDstQaRunId::~yunoPicoDstQaRunId() {}

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::CreateVtxHistogram(struct Histogram *hist, const char *text) {
  double mPosV[]    = {200., -4.,   4.};
  double mPosZ[]    = {100,   196., 204.};
  double mRefMult[] = {600.,  0.,   600.};
 
  vector<struct strHisto1D> histo1D {
    { "z Vertex position;z (cm);Counts",
      mPosZ[0], mPosZ[1], mPosZ[2] },
    { "Reference multiplicity;N;Counts",
      mRefMult[0], mRefMult[1], mRefMult[2] },
    { "Primary multiplicity;N;Counts",
      mRefMult[0], mRefMult[1], mRefMult[2] }
  };
  vector<struct strHisto2D> histo2D {
    { "y Vs x Vertex position;x (cm);y (cm)",
      mPosV[0], mPosV[1], mPosV[2], mPosV[0], mPosV[1], mPosV[2] },
    { "Primary mutiplicity Vs Reference multiplicity;N_{RefMult};N_{PrimMult}",
      mRefMult[0], mRefMult[1], mRefMult[2], mRefMult[0], mRefMult[1], mRefMult[2] }
  };
  
  for(int i = 0; i < (int)(histo2D.size()); i++ ) {
    hist->H2D.push_back( new TH2F(Form("%s_H2D_%i",text,i),
                                  Form("%s",histo2D.at(i).name),
                                  histo2D.at(i).x.nBins, histo2D.at(i).x.lo, histo2D.at(i).x.hi,
                                  histo2D.at(i).y.nBins, histo2D.at(i).y.lo, histo2D.at(i).y.hi) );
  }
  for(int i = 0; i < (int)(histo1D.size()); i++ ) {
    hist->H1D.push_back( new TH1D(Form("%s_H1D_%i",text,i),
                                  Form("%s",histo1D.at(i).name),
                                  histo1D.at(i).nBins, histo1D.at(i).lo, histo1D.at(i).hi) );
  }


} //CreateVtxHistogram
/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::CreateTrackHistogram(struct Histogram *hist, const char *text) {
  double mMom[]     = {500.,  0.,   2.2};
  double mdEdX[]    = {1000., 0.,   12.};
  double mEtaR[]    = {600.,  -3.,   0.5};
  double mPhi[]     = {640., -3.2,  3.2};
  double mNHits[]   = {100.,  0.,   100.};
  double mDca[]     = {40.,   0.,   4.};
 
  vector<struct strHisto1D> histo1D {
    { "#eta of particle track;#eta (rad);Counts",
      mEtaR[0], mEtaR[1], mEtaR[2] },
    { "#phi of particle track;#phi (rad);Counts",
      mPhi[0],mPhi[1],mPhi[2] },
    { "p_{T} of particle track;p_{T} (GeV/c);Counts",
      mMom[0],mMom[1],mMom[2] },
    { "nHitsFit of particle track;nHitsFit;Counts",
      mNHits[0], mNHits[1], mNHits[2] },
    { "DCA Mag particle track to PV;dca_{mag} (cm);Counts",
      mDca[0],mDca[1],mDca[2] },
    { "Signed DCA Perp particle track to PV;signed dca_{perp} (cm);Counts",
      600.,-3., 3. },
    { "dEdX of particle track;dEdX (keV/cm);Counts",
      mdEdX[0], mdEdX[1], mdEdX[2] }
  };

  for(int i = 0; i < (int)(histo1D.size()); i++ ) {
    hist->H1D.push_back( new TH1D(Form("%s_H1D_%i",text,i),
                                  Form("%s",histo1D.at(i).name),
                                  histo1D.at(i).nBins, histo1D.at(i).lo, histo1D.at(i).hi) );
  }
} //CreateTrackHistogram

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::FillVtxHistogram(struct Histogram *hist) {
  vector<double> varH1D {
    mVertPositionZ,
    mRefMult,
    mNumberOfPT
  };
  for (int i = 0; i < (int)(varH1D.size()); i++) {
    hist->H1D.at(i)->Fill( varH1D.at(i) );
  }
  vector<double> varH2D {
    mVertPositionX, mVertPositionY,
    mRefMult, mNumberOfPT
  };
  for (int i = 0; i < (int)(varH2D.size())/2; i++) {
    hist->H2D.at(i)->Fill( varH2D.at(2*i), varH2D.at(2*i + 1));
  }
} //FillVtxHistogram

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::FillTrackHistogram(struct Histogram *hist) {
  vector<double> varH1D {
    mEta,
    mPhi,
    mPt,
    mNHitsFit,
    mDcaMag,
    mSDcaPerp,
    mDeDx
  };
  for (int i = 0; i < (int)(varH1D.size()); i++) {
    hist->H1D.at(i)->Fill( varH1D.at(i) );
  }
 
} //FillTrackHistogram

/////////////////////////////////////////////////////
void yunoPicoDstQaRunId::PerformQaRunId(const char *inFile, const char *outFile) {
  mOutFile = new TFile(outFile, "RECREATE");

  cout << "Creating Vtx histograms" << endl;
  CreateVtxHistogram(&mBCVtx, "mBCVtx");
  CreateVtxHistogram(&mACVtx, "mACVtx");
  cout << "Creating Trc histograms" << endl;
  CreateTrackHistogram(&mBCtrc, "mBCtrc");
  CreateTrackHistogram(&mACtrc, "mACtrc");
  cout << "Creating TProfiles" << endl;
  vector<const char *> histoNames {
    "<Number of Primary Tracks> vs Run number;RunId;<Number of PT>",
    "<#eta> vs Run number;RunId;<#eta>",
    "<#phi> vs Run number;RunId;<#phi>",
    "<sDCA_{xy}> vs Run number;RunId;<sDCA_{xy}> (cm)",
    "<dE/dx> vs Run number;RunId;<dE/dx>"
  };
  for(int i = 0; i < (int)histoNames.size(); i++ ) {
    VertexProfile.push_back(new TProfile(Form("VertexProfile_%i",i),
                                         Form("%s",histoNames.at(i)),
                                         mRunIdBins, mRunIdVal[0], mRunIdVal[1]));
  }

  TH2D *h2runidvssdcaxy = new TH2D("h2runidvssdcaxy","RunId v.s. sDCAxy;RunId;sDCAxy",mRunIdBins, mRunIdVal[0], mRunIdVal[1],400,-1.0,1.0);
  h2runidvssdcaxy->Sumw2();
  cout << "creating done" << endl;

  picoReader = new StPicoDstReader(inFile);
  picoReader->Init();

  picoReader->SetStatus("*",0);
  picoReader->SetStatus("Event",1);
  picoReader->SetStatus("Track",1);
  picoReader->SetStatus("BTofHit",1);
  picoReader->SetStatus("BTofPidTraits*",1);

  if( !picoReader->chain() )
  {
    cout << "No chain has been found." << endl;
  }

  EventVariablesClear();
  mNEvents = picoReader->chain()->GetEntries();
  cout << "Number of events to read: " << mNEvents << endl;

  for(unsigned int iEvent = 0; iEvent < mNEvents; iEvent++) {

    bool readEvent = picoReader->readPicoEvent(iEvent);
    if( !readEvent )
    {
      cout << "Can't read event." << endl;
      break;
    }

    dst = picoReader->picoDst();
    event = dst->event();
    if( !event )
    {
      cout << "Can't find event." << endl;
      break;
    }

    mRunId = event->runId();
    mRefMult = event->refMult();
    mNumberOfPT = event->numberOfPrimaryTracks();
    mVertPositionX = event->primaryVertex().x();
    mVertPositionY = event->primaryVertex().y();
    mVertPositionZ = event->primaryVertex().z();
    mVertR = sqrt(pow(mVertPositionX-mShift[0],2) + pow(mVertPositionY-mShift[1],2));

    iBeforeAllCut++;

    if( !CutTriggerId() )   continue;
    iTriggerId++;

    FillVtxHistogram(&mBCVtx);
    if( mVertPositionZ < mVzCut[0] || mVertPositionZ > mVzCut[1])   continue;
    iZVertexCut++;
    if( mVertR > mXYSqrCut )   continue;
    iXYSqrCut++;

    MeanTrackVariablesClear();
    for (int iPrTr = 0; iPrTr < dst->numberOfTracks(); iPrTr++) {
      TrackVariablesClear();
      mPrimTrack = dst->track(iPrTr);

      if ( !mPrimTrack ) continue;
      if ( !mPrimTrack->isPrimary() ) continue;

      mPt = mPrimTrack->pPt();
      mDcaMag = mPrimTrack->gDCA(event->primaryVertex()).Mag();
      mDcaPerp = mPrimTrack->gDCAxy(event->primaryVertex().X(), event->primaryVertex().Y());
      StPicoPhysicalHelix mHelix = mPrimTrack->helix(mPrimTrack->gMom().Mag());
      mSDcaPerp = mHelix.geometricSignedDistance(event->primaryVertex().X(), event->primaryVertex().Y());
      mPhi = mPrimTrack->pMom().Phi();
      mEta = mPrimTrack->pMom().Eta();
      mDeDx = mPrimTrack->dEdx();
      mNHitsFit = mPrimTrack->nHitsFit();
      
      FillTrackHistogram(&mBCtrc);
      if(mNHitsFit < mNHitsCut[0] || mNHitsFit > mNHitsCut[1]) continue;
      if(mPt < mMomPtCut[0] || mPt > mMomPtCut[1]) continue;
      if(mDcaMag < mDcaCut[0] || mDcaMag > mDcaCut[1] ) continue;
      if(mEta < mEtaCut[0] || mEta > mEtaCut[1] ) continue;
      FillTrackHistogram(&mACtrc);
      h2runidvssdcaxy->Fill(mRunId,mSDcaPerp);
  
      map<string, double> mTrackVars {
        {"mEta", mEta},
        {"mPhi", mPhi},
        {"mSDcaPerp", mSDcaPerp},
        {"mDeDx", mDeDx}
      };
      for (auto it = mTrackVars.begin(); it != mTrackVars.end(); it++) {
        mMeanTrackVars[it->first] += it->second;
      }
      mGoodPrimTracks++;
    } //for (int iPrTr = 0; iPrTr < mNGlobTracks; iPrTr++)

    FillVtxHistogram(&mACVtx);

    if (mGoodPrimTracks > 0) {
      for (auto it = mMeanTrackVars.begin(); it != mMeanTrackVars.end(); it++) {
        it->second /= mGoodPrimTracks;
      }
    }//if (mGoodPrimTracks > 0) 
    vector<double> varProf {
      mNumberOfPT,
      mMeanTrackVars["mEta"],
      mMeanTrackVars["mPhi"],
      mMeanTrackVars["mSDcaPerp"],
      mMeanTrackVars["mDeDx"]
    };
    for (int i = 0; i < (int)varProf.size(); i++) {
      VertexProfile.at(i)->Fill( mRunId, varProf[i]);
    }
    for (auto it = mMeanTrackVars.begin(); it != mMeanTrackVars.end(); it++) {
      it->second = 0.;
    }

  } //for(Long64_t iEvent=0; iEvent<events2read; iEvent++)
  cout << "iBeforeAllCut = " << iBeforeAllCut << "\n"
       << "iTriggerId = " << iTriggerId << "\n"
       << "iZVertexCut = " << iZVertexCut << "\n"
       << "iXYSqrCut = " << iXYSqrCut << endl;
 
  cout << "Writing histograms to the output file:";
  mOutFile->Write();
  cout << "\t[DONE]" << endl;
  cout << "Closing output file:";
  mOutFile->Close();
  cout << "\t[DONE]" << endl;
  picoReader->Finish();
} //PerformQaRunId

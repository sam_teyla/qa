#include <TFile.h>
#include "TROOT.h"
#include "TSystem.h"
#include "TChain.h"
#include "TTree.h"

R__LOAD_LIBRARY(/home/yuno/Work/QA/codes/picoQA/StPicoEvent/libStPicoDst.so)
R__LOAD_LIBRARY(/home/yuno/Work/QA/codes/picoQA/yunoPicoDstQa_cxx.so)

const int trigMas[] = {580001,580011,580021};    
const int badRunsMas[] = {18153057,18153063,18154037,18154038,
  18154039,18157003,18157011,18158020,
  18158021,18161005,18161021,18164044,
  18165039,18166033,18166052,18167014,
  18167015,18167016,18167017,18167018,
  18167041,18168015,18169036,18170021};
void RunAnalyzer(const char *InFile = "st_physics_18155041_raw_0000003.picoDst.root",//"test.list",//"st_physics_12143001_raw_0010001.picoDst.root",
                 const char *OutFile = "testQa.root") {
  
  gROOT->ProcessLine("#define _VANILLA_ROOT_");
  // std::cout << "Loading shared libraries" << std::endl;
  // gROOT->LoadMacro("$STAR/StRoot/StMuDSTMaker/COMMON/macros/loadSharedLibraries.C");
  // loadSharedLibraries();
  gSystem->Load("StPicoEvent");
  gSystem->Load("yunoPicoDstQa");
  yunoPicoDstQa *QA = new yunoPicoDstQa();
  std::vector<int> triggers;
  for (int iTr = 0; iTr < (int)(sizeof (trigMas) / sizeof (*trigMas)); iTr++) {
    triggers.push_back(trigMas[iTr]);
  } 
  QA->SetTriggerIds(triggers);
  std::vector<int> badRuns;
  for (int iTr = 0; iTr < (int)(sizeof (badRunsMas) / sizeof (*badRunsMas)); iTr++) {
    badRuns.push_back(badRunsMas[iTr]);
  }   
  QA->SetVzCutRange(-30., 30.);
  QA->SetVzDiffCutRange(200.);
  QA->SetShiftXY(0., 0.);
  QA->SetXYSqrCutRange(2.);
  QA->SetNHitsCutRange(15, 100);
  QA->SetMomCutRange(0., 100.);
  QA->SetMomPtCutRange(0.15, 1.45);
  QA->SetEtaCutRange(-1., 1.);
  QA->SetDcaCutRange(0., 3.);
  QA->SetPid(3);
  
  
  QA->PerformQaRegular(InFile, OutFile);


  delete QA;

}

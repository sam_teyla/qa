//////////////////////////////////////////////////////
// This class was developed by Eugenia Khyzhniak
// eugenia.sh.el@gmail.com
//////////////////////////////////////////////////////

#ifndef yunoPicoDstQaRunId_hh
#define yunoPicoDstQaRunId_hh

#include <iostream>
#include <map>

#include "TROOT.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH2.h"


#define _VANILLA_ROOT_
#include "StPicoEvent/StPicoDstReader.h"
#include "StPicoEvent/StPicoDst.h"
#include "StPicoEvent/StPicoEvent.h"
#include "StPicoEvent/StPicoTrack.h"
#include "StPicoEvent/StPicoBTofPidTraits.h"

using namespace std;

/////////////////////////////////////////////////////
class yunoPicoDstQaRunId {
public :
  yunoPicoDstQaRunId();
  ~yunoPicoDstQaRunId();

  void PerformQaRunId(const char *inFile, const char *outFile);

  TFile      *mOutFile;

  StPicoDstReader* picoReader;
  StPicoTrack *mPrimTrack;
  StPicoDst *dst;
  StPicoEvent *event;
  StPicoBTofPidTraits *trait;

  vector <int> mTriggerId;
  int mRunId, mRunIdVal[2], mRunIdBins, mGoodPrimTracks, iBeforeAllCut, iTriggerId, iZVertexCut, iXYSqrCut;
  unsigned int mNEvents;
  double  mRefMult, mNumberOfPT, mVertPositionX, mVertPositionY, mVertPositionZ, mVertR;
  double mPt, mDcaMag, mDcaPerp, mSDcaPerp, mPhi, mEta, mDeDx, mNHitsFit;
  map<string, double> mMeanTrackVars;
  double mVzCut[2], mShift[2], mXYSqrCut;
  double mNHitsCut[2], mMomPtCut[2], mEtaCut[2], mDcaCut[2];

  vector<TProfile *> VertexProfile;
  struct strHisto1D {const char *name; double nBins, lo, hi;  };
  struct strHisto2D {const char *name; struct opt {double nBins, lo, hi;} x,y;  };
  struct Histogram {
    vector<TH1D*> H1D;
    vector<TH2F*> H2D;
  } mBCVtx, mACVtx, mBCtrc, mACtrc;

  bool CutTriggerId();

  void EventVariablesClear();
  void MeanTrackVariablesClear();
  void TrackVariablesClear();

  void CreateVtxHistogram(struct Histogram *hist, const char *text);
  void CreateTrackHistogram(struct Histogram *hist, const char *text);
  void FillVtxHistogram(struct Histogram *hist);
  void FillTrackHistogram(struct Histogram *hist);

  void SetTriggerIds(vector<int> trig = {350003,350013,350023,350033,350043});
  void SetRunIdRange(int first = 12122000, int last = 12172000);
  void SetVzCutRange(double lo = -30., double hi = 30.);
  void SetShiftXY(double x = 0., double y = 0.);
  void SetXYSqrCutRange(double cut = 2.);
  void SetNHitsCutRange(double lo = 15., double hi = 60.);
  void SetMomPtCutRange(double lo = 0.15, double hi = 10.);
  void SetEtaCutRange(double lo = -1., double hi = 1.);
  void SetDcaCutRange(double lo = 0., double hi = 3.);

  ClassDef(yunoPicoDstQaRunId, 1)
};
#endif



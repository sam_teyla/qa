#define yunoPicoDstQa_cxx
#include "yunoPicoDstQa.h"

ClassImp(yunoPicoDstQa)

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetBadRunIds(vector<int> runIds) {
  mBadRunId = runIds;
};

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetTriggerIds(vector<int> trig) {
  mTriggerId = trig;
};

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetVzCutRange(float lo, float hi) {
  mVzCut[0] = lo; mVzCut[1] = hi;
} //SetVzCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetXYZCutRange(float cut) {
  mXYZCut = cut;
} //SetXYZCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetVzDiffCutRange(float cut) {
  mVzDiffCut = cut;
} //SetVzDiffCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetShiftXY(float x, float y) {
  mShift[0] = x; mShift[1] = y;
}//SetShiftXY

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetXYSqrCutRange(float cut) {
  mXYSqrCut = cut;
} //SetXYSqrCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetNHitsCutRange(float lo, float hi) {
  mNHitsCut[0] = lo;   mNHitsCut[1] = hi;
} //SetNHitsCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetRCutRange(float lo, float hi) {
  mRCut[0] = lo;      mRCut[1] = hi;
} //SetRCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetMomCutRange(float lo, float hi) {
  mMomCut[0] = lo;    mMomCut[1] = hi;
} //SetMomCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetMomPtCutRange(float lo, float hi) {
  mMomPtCut[0] = lo;  mMomPtCut[1] = hi;
} //SetMomPtCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetEtaCutRange(float lo, float hi) {
  mEtaCut[0] = lo;     mEtaCut[1] = hi;
} //SetEtaCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetDcaCutRange(float lo, float hi) {
  mDcaCut[0] = lo;      mDcaCut[1] = hi;
} //SetDcaCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetPid(int pid) {
  //0 - just TPC,
  //1 - just TOF,
  //2 - TPC < particleMom, TOF >= particleMom
  //3 - (TPC+TOF) || TPC (pTr)
  mIdentType = pid;
} //SetPid

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetProtonCutRange(float loMassSqr, float hiMassSqr,
                                      float loInvBeta, float hiInvBeta,
                                      float mom,
                                      double nSigma0, double nSigma1,
                                      double nSigma2, float nSigma3) {
  mProtonMassSqr[0] = loMassSqr;  mProtonMassSqr[1] = hiMassSqr;
  mInvBetaProton[0] = loInvBeta;  mInvBetaProton[1] = hiInvBeta;
  mProtonMom = mom;
  mNSigmaProton[0] = nSigma0;  mNSigmaProton[1] = nSigma1;
  mNSigmaProton[2] = nSigma2;  mNSigmaProton[3] = nSigma3;
} //SetProtonCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetKaonCutRange(float loMassSqr, float hiMassSqr,
                                    float loInvBeta, float hiInvBeta,
                                    float mom,
                                    double nSigma0, double nSigma1,
                                    double nSigma2, float nSigma3) {
  mKaonMassSqr[0] = loMassSqr;  mKaonMassSqr[1] = hiMassSqr;
  mInvBetaKaon[0] = loInvBeta;  mInvBetaKaon[1] = hiInvBeta;
  mKaonMom = mom;
  mNSigmaKaon[0] = nSigma0;  mNSigmaKaon[1] = nSigma1;
  mNSigmaKaon[2] = nSigma2;  mNSigmaKaon[3] = nSigma3;
} //SetKaonCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::SetPionCutRange(float loMassSqr, float hiMassSqr,
                                    float loInvBeta, float hiInvBeta,
                                    float mom,
                                    double nSigma0, double nSigma1,
                                    double nSigma2, float nSigma3) {
  mPionMassSqr[0] = loMassSqr;  mPionMassSqr[1] = hiMassSqr;
  mInvBetaPion[0] = loInvBeta;  mInvBetaPion[1] = hiInvBeta;
  mPionMom = mom;
  mNSigmaPion[0] = nSigma0;  mNSigmaPion[1] = nSigma1;
  mNSigmaPion[2] = nSigma2;  mNSigmaPion[3] = nSigma3;
} //SetPionCutRange

/////////////////////////////////////////////////////
void yunoPicoDstQa::CreateOutHistogram() {
  hNeventsAfterEachCut = new TH1D("hNeventsAfterEachCut",
                                  "Number of event after each cut;;Number of events",
                                  9,0.,9.);
  hNtracksAfterEachCut = new TH1D("hNtracksAfterEachCut",
                                  "Number of tracks after each cut;;Number of tracks",
                                  7,0.,7.);
  hRefMultBeforeRunIdCut = new TH1D("hRefMultBeforeRunIdCut",
                                    "Reference multiplicity before cut to RunId;RefMult;Counts",
                                    600,0.,600.);
} //CreateOutHistogram

/////////////////////////////////////////////////////
void yunoPicoDstQa::CreateVtxHistogram(struct Histogram *hist, const char *text) {
  double mPosV[] = {200., 10., 100., 2.};
  double mRefMult[] = {600., 0., 600.};
  double mTofMult[] = {3200., 0., 3200.};
  double mAdc[] = {400., 0., 4000.};

  vector<struct strHisto1D> histo1D {
    { "x Vertex position;x (cm);Counts",
      mPosV[0], -mPosV[3], mPosV[3] },
    { "y Vertex position;y (cm);Counts",
      mPosV[0], -mPosV[3], mPosV[3] },
    { "z Vertex position;z (cm);Counts",
      mPosV[0], -mPosV[2], mPosV[2] },
    { "VpdVz Vertex position;z (cm);Counts",
      mPosV[0], -mPosV[2], mPosV[2] },
    { "VpdVz-Vz;N_{VpdVz-Vz};Counts",
      mPosV[0], -mPosV[1], mPosV[1] },
    { "Reference multiplicity;N;Counts",
      mRefMult[0], mRefMult[1], mRefMult[2] },
    { "Tof multiplicity;N;Counts",
      mTofMult[0], mTofMult[1], mTofMult[2] },
    { "AdcSum ZDC East;AdcSum_{ZDCE};Counts",
      mAdc[0], mAdc[1], mAdc[2] },
    { "AdcSum ZDC West;AdcSum_{ZDCW};Counts",
      mAdc[0], mAdc[1], mAdc[2] },
    { "ZDC Rate;Rate_{ZDC};Counts",
      mAdc[0], mAdc[1], 10000. },
    { "AdcSum BBC East;AdcSum_{BBCE};Counts",
      4*mAdc[0], mAdc[1], 8.2*mAdc[2] },
    { "AdcSum BBC West;AdcSum_{BBCW};Counts",
      4*mAdc[0], mAdc[1], 8.2*mAdc[2] },
    { "BBC Rate;Rate_{BBC};Counts",
      mAdc[0], mAdc[1], 1e5 }
  };
  vector<struct strHisto2D> histo2D {
    { "y Vs x Vertex position;x (cm);y (cm)",
      mPosV[0], -mPosV[3], mPosV[3], mPosV[0], -mPosV[3], mPosV[3] },
    { "z Vs x Vertex position;x (cm);z (cm)",
      mPosV[0], -mPosV[3], mPosV[3], mPosV[0], -mPosV[2], mPosV[2] },
    { "y Vs z Vertex position;z (cm);y (cm)",
      mPosV[0], -mPosV[3], mPosV[3], mPosV[0], -mPosV[1], mPosV[1] },
    { "VpdVz Vs Vz Vertex position;z (cm);z_{VPD} (cm)",
      mPosV[0], -mPosV[2], mPosV[2], mPosV[0], -mPosV[2], mPosV[2] },
    { "TOF mutiplicity Vs Reference multiplicity;N_{RefMult};N_{TofMult}",
      mRefMult[0], mRefMult[1], mRefMult[2], mTofMult[0], mTofMult[1], mTofMult[2] },
    { "x_{primary vertex} vs Reference multiplicity;N_{RefMult};x (cm)",
      mRefMult[0], mRefMult[1], mRefMult[2], mPosV[0], -mPosV[3], mPosV[3] },
    { "y_{primary vertex} vs Reference multiplicity;N_{RefMult};y (cm)",
      mRefMult[0], mRefMult[1], mRefMult[2], mPosV[0], -mPosV[3], mPosV[3] },
    { "z_{primary vertex} vs Reference multiplicity;N_{RefMult};z (cm)",
      mRefMult[0], mRefMult[1], mRefMult[2], mPosV[0], -mPosV[2], mPosV[2] },
    { "Adc_{ZDCE} vs Reference multiplicity;N_{RefMult};Adc_{ZDCE}",
      mRefMult[0], mRefMult[1], mRefMult[2], mAdc[0], mAdc[1], mAdc[2] },
    { "Adc_{ZDCW} vs Reference multiplicity;N_{RefMult};Adc_{ZDCW}",
      mRefMult[0], mRefMult[1], mRefMult[2], mAdc[0], mAdc[1], mAdc[2] },
    { "Adc_{BBCE} vs Reference multiplicity;N_{RefMult};Adc_{BBCE}",
      mRefMult[0], mRefMult[1], mRefMult[2], 4*mAdc[0], mAdc[1], 8.2*mAdc[2] },
    { "Adc_{BBCW} vs Reference multiplicity;N_{RefMult};Adc_{BBCW}",
      mRefMult[0], mRefMult[1], mRefMult[2], 4*mAdc[0], mAdc[1], 8.2*mAdc[2] },
    { "AdcSum ZDC East Vs AdcSum ZDC West;AdcSum_{ZDCW};AdcSum_{ZDCE}",
      mAdc[0], mAdc[1], mAdc[2], mAdc[0], mAdc[1], mAdc[2] },
    { "AdcSum BBC East Vs AdcSum BBC West;AdcSum_{BBCW};AdcSum_{BBCE}",
      4*mAdc[0], mAdc[1], 8.2*mAdc[2], 4*mAdc[0], mAdc[1], 8.2*mAdc[2] },
  };
  for(int i = 0; i < (int)(histo2D.size()); i++ ) {
    hist->H2D.push_back( new TH2F(Form("%s_H2D_%i",text,i),
                                  Form("%s",histo2D.at(i).name),
                                  histo2D.at(i).x.nBins, histo2D.at(i).x.lo, histo2D.at(i).x.hi,
                                  histo2D.at(i).y.nBins, histo2D.at(i).y.lo, histo2D.at(i).y.hi) );
  }
  for(int i = 0; i < (int)(histo1D.size()); i++ ) {
    hist->H1D.push_back( new TH1D(Form("%s_H1D_%i",text,i),
                                  Form("%s",histo1D.at(i).name),
                                  histo1D.at(i).nBins, histo1D.at(i).lo, histo1D.at(i).hi) );
  }

} //CreateVtxHistogram

/////////////////////////////////////////////////////
void yunoPicoDstQa::CreateTrackHistogram(struct Histogram *hist, const char *text) {
  double mMom[]   = {500.,    0.,  2.2};
  double mdEdX[]  = {1000.,   0.,  12.};
  double mSigma[] = {1000., -10.,  10.};
  double mEta[]   = {600.,   -2.,   2.};
  double mPhi[]   = {640.,  -3.2,  3.2};
  double mNHits[] = {50.,   -0.5, 49.5};
  double mDca[]   = {100.,    0.,  10.};
  double mBeta[]  = {300.,   0.,   3.};
  double mMass[]  = {500.,  -0.5,   1.5};
  double mChi[]   = {20.,   0.,   20.};

  vector<struct strHisto1D> histo1D {
    { "p_{x} of particle track;p_{x} (GeV/c);Counts",
      mMom[0],-mMom[2],mMom[2] },
    { "p_{y} of particle track;p_{y} (GeV/c);Counts",
      mMom[0],-mMom[2],mMom[2] },
    { "p_{z} of particle track;p_{z} (GeV/c);Counts",
      mMom[0],-mMom[2],mMom[2] },
    { "p_{T} of particle track;p_{T} (GeV/c);Counts",
      mMom[0],mMom[1],mMom[2] },
    { "p of particle track;p (GeV/c);Counts",
      mMom[0],mMom[1],mMom[2] },
    { "Charge;Charge;Counts",
      6,-1.5,1.5 },
    { "nHits of particle track;nHits;Counts",
      mNHits[0], mNHits[1], mNHits[2] },
    { "nHitsFit of particle track;nHitsFit;Counts",
      mNHits[0], mNHits[1], mNHits[2] },
    { "nHitsPoss of particle track;nHitsPoss;Counts",
      mNHits[0], mNHits[1], mNHits[2] },
    { "NHitsFit/NHitsPoss for particle track;R_{Hits};Counts",
      mNHits[0], 0., 1.5 },
    { "#eta of particle track;#eta (rad);Counts",
      mEta[0], mEta[1], mEta[2] },
    { "#phi of particle track;#phi (rad);Counts",
      mPhi[0],mPhi[1],mPhi[2] },
    { "Dca X particle track to PV;dca_{x} (cm);Counts",
      mDca[0],mDca[1],mDca[2] },
    { "Dca Y particle track to PV;dca_{y} (cm);Counts",
      mDca[0],mDca[1],mDca[2] },
    { "Dca Z particle track to PV;dca_{z} (cm);Counts",
      mDca[0],mDca[1],mDca[2] },
    { "Dca Mag particle track to PV;dca_{mag} (cm);Counts",
      mDca[0],mDca[1],mDca[2] },
    { "Dca Perp particle track to PV;dca_{perp} (cm);Counts",
      mDca[0],mDca[1],mDca[2] },
    { "dEdX of particle track;dEdX (keV/cm);Counts",
      mdEdX[0], mdEdX[1], mdEdX[2] },
    { "1/#beta of particle track;1/#beta;Counts",
      mBeta[0], mBeta[1], mBeta[2] },
    { "#beta of particle track;#beta;Counts",
      mBeta[0], mBeta[1], mBeta[2] },
    { "chi2 of particle track;chi2;Counts",
      mChi[0], mChi[1], mChi[2] },
    { "Number of Hits DeDx;NhitsDeDx;Counts",
      mNHits[0], mNHits[1], mNHits[2] }
  };
  vector<struct strHisto2D> histo2D {
    { "NHitsFit vs momentum of particle track;pQ (GeV/c);NhitsFit)",
      mMom[0], -mMom[2], mMom[2],
      mNHits[0], mNHits[1], mNHits[2] },
    { "1/#beta vs momentum of particle track;pQ (GeV/c);1/#beta",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], mBeta[1], mBeta[2] },
    { "Mass square vs momentum of particle track;pQ (GeV/c);m^{2} (GeV/c^{2})",
      mMom[0], -mMom[2], mMom[2],
      mMass[0], mMass[1], mMass[2] },
    { "dEdX vs momentum of particle track;pQ (GeV/c);dEdX (keV/cm)",
      mMom[0],-mMom[2],mMom[2],
      mdEdX[0], mdEdX[1], mdEdX[2] },
    { "#eta Vs #phi of particle track;#eta (rad);#phi (rad)",
      mPhi[0],mPhi[1],mPhi[2],
      mEta[0],mEta[1],mEta[2] },
    { "Number of sigma Pion vs momentum of particle track;pQ (GeV/c);N#sigma_{Pion}",
      mMom[0],-mMom[2],mMom[2],
      mSigma[0], mSigma[1], mSigma[2] },
    { "Number of sigma Kaon vs momentum of particle track;pQ (GeV/c);N#sigma_{Kaon}",
      mMom[0],-mMom[2],mMom[2],
      mSigma[0], mSigma[1], mSigma[2] },
    { "Number of sigma Proton vs momentum of particle track;pQ (GeV/c);N#sigma_{Proton}",
      mMom[0],-mMom[2],mMom[2],
      mSigma[0], mSigma[1], mSigma[2] },
    { "Number of sigma Electron vs momentum of particle track;pQ (GeV/c);N#sigma_{Electron}",
      mMom[0],-mMom[2],mMom[2],
      mSigma[0], mSigma[1], mSigma[2] },
    { "1/#beta - 1/#beta_expected of proton vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{proton}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] },
    { "1/#beta - 1/#beta_expected of kaon vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{kaon}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] },
    { "1/#beta - 1/#beta_expected of pion vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{pion}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] },
    { "1/#beta - 1/#beta_expected of electron vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{electron}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] },
    { "#eta Vs p_{T} of particle track;p_{T} (GeV/c);#eta (rad)",
      mMom[0],mMom[1],mMom[2],
      mEta[0],mEta[1],mEta[2] },
    { "#phi Vs p_{T} of particle track;p_{T} (GeV/c);#phi (rad)",
      mMom[0],mMom[1],mMom[2],
      mPhi[0],mPhi[1],mPhi[2] },
    { "N#sigma_{Pion} vs Mass Square;m^{2} (GeV/c^{2});N#sigma_{Pion}",
      mMass[0], mMass[1], mMass[2],
      mSigma[0], mSigma[1], mSigma[2] },
    { "N#sigma_{Kaon} vs Mass Square;m^{2} (GeV/c^{2});N#sigma_{Kaon}",
      mMass[0], mMass[1], mMass[2],
      mSigma[0], mSigma[1], mSigma[2] },
    { "N#sigma_{Proton} vs Mass Square;m^{2} (GeV/c^{2});N#sigma_{Proton}",
      mMass[0], mMass[1], mMass[2],
      mSigma[0], mSigma[1], mSigma[2] },
    { "N#sigma_{Electron} vs Mass Square;m^{2} (GeV/c^{2});N#sigma_{Electron}",
      mMass[0], mMass[1], mMass[2],
      mSigma[0], mSigma[1], mSigma[2] }
  };
  for(int i = 0; i < (int)(histo1D.size()); i++ ) {
    hist->H1D.push_back( new TH1D(Form("%s_H1D_%i",text,i),
                                  Form("%s",histo1D.at(i).name),
                                  histo1D.at(i).nBins, histo1D.at(i).lo, histo1D.at(i).hi) );
  }
  for(int i = 0; i < (int)(histo2D.size()); i++ ) {
    hist->H2D.push_back( new TH2F(Form("%s_H2D_%i",text,i),
                                  Form("%s",histo2D.at(i).name),
                                  histo2D.at(i).x.nBins, histo2D.at(i).x.lo, histo2D.at(i).x.hi,
                                  histo2D.at(i).y.nBins, histo2D.at(i).y.lo, histo2D.at(i).y.hi) );
  }

} //CreateTrackHistogram


/////////////////////////////////////////////////////
void yunoPicoDstQa::CreatePidHistogram(struct Histogram *hist, const char *text) {
  double mMom[]   = {500.,    0.,  2.2};
  double mdEdX[]  = {1000.,   0.,  12.};
  double mEta[]   = {600.,   -2.,   2.};
  double mPhi[]   = {640.,  -3.2,  3.2};
  double mBeta[]  = {300.,   0,   3.};
  double mMass[]  = {500.,  -0.5,   1.5};
  double mNHits[] = {100.,   -0.5, 99.5};
  double mDca[]   = {100.,    0.,  10.};

  vector<struct strHisto1D> histo1D {
    { "p of particle track;p (GeV/c);Counts",
      mMom[0],mMom[1],mMom[2] },
    { "#eta  of particle track;#eta;Counts",
      mEta[0],mEta[1],mEta[2] },
    { "#phi  of particle track;#phi;Counts",
      mPhi[0],mPhi[1],mPhi[2] },
    { "1/#beta of particle track;1/#beta;Counts",
      mBeta[0], mBeta[1], mBeta[2] },
    { "#beta of particle track;#beta;Counts",
      mBeta[0], mBeta[1], mBeta[2] },
    { "Number of Hits DeDx;NhitsDeDx;Counts",
      mNHits[0], mNHits[1], mNHits[2] },
    { "Number of Hits Fit;NhitsFit;Counts",
      mNHits[0], mNHits[1], mNHits[2] },
    { "dEdX of particle track;dEdX (keV/cm);Counts",
      mdEdX[0], mdEdX[1], mdEdX[2] },
    { "Dca Mag particle track to PV;dca_{mag} (cm);Counts",
      mDca[0],mDca[1],mDca[2] },
    { "Dca Perp particle track to PV;dca_{perp} (cm);Counts",
      mDca[0],mDca[1],mDca[2] }
  };
  vector<struct strHisto2D> histo2D {
    { "1/#beta vs momentum of particle track;pQ (GeV/c);1/#beta",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], mBeta[1], mBeta[2] },
    { "MassSqr vs momentum of particle track;pQ (GeV/c);m^{2} (GeV/c^{2})",
      mMom[0], -mMom[2], mMom[2],
      mMass[0], mMass[1],mMass[2] },
    { "#eta Vs #phi of particle track;#eta (rad);#phi (rad)",
      mPhi[0],mPhi[1],mPhi[2],
      mEta[0],mEta[1],mEta[2] },
    { "dEdX vs momentum of particle track;pQ (GeV/c);dEdX (keV/cm)",
      mMom[0],-mMom[2],mMom[2],
      mdEdX[0], mdEdX[1], mdEdX[2] },
    { "1/#beta - 1/#beta_expected of proton vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{proton}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] },
    { "1/#beta - 1/#beta_expected of kaon vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{kaon}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] },
    { "1/#beta - 1/#beta_expected of pion vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{pion}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] },
    { "1/#beta - 1/#beta_expected of electron vs momentum of particle track;pQ (GeV/c);1/#beta - 1/#beta_expected_{electron}",
      mMom[0],-mMom[2],mMom[2],
      mBeta[0], -mBeta[2], mBeta[2] }
  };
  for(int i = 0; i < (int)(histo1D.size()); i++ ) {
    hist->H1D.push_back( new TH1D(Form("%s_H1D_%i",text,i),
                                  Form("%s",histo1D[i].name),
                                  histo1D[i].nBins, histo1D[i].lo, histo1D[i].hi) );
  }
  for(int i = 0; i < (int)(histo2D.size()); i++ ) {
    hist->H2D.push_back( new TH2F(Form("%s_H2D_%i",text,i),
                                  Form("%s",histo2D.at(i).name),
                                  histo2D.at(i).x.nBins, histo2D.at(i).x.lo, histo2D.at(i).x.hi,
                                  histo2D.at(i).y.nBins, histo2D.at(i).y.lo, histo2D.at(i).y.hi) );
  }

} //CreatePidHistogram
/////////////////////////////////////////////////////
void yunoPicoDstQa::EventVariablesClear() {
  //Number of events
  mNEvents = 0;

  //Event counts
  iBeforeAllCut = 0;
  iRunId = 0;
  iTriggerId = 0;
  iRefMultCut = 0;
  iZVertexCut = 0;
  iXYZCut = 0;
  iVzDiffCut = 0;
  iXYSqrCut = 0;

  //Event variables
  mRunId = -999.;
  mRefMult = -999.;
  mNumberOfPT = -999.;
  mNumberOfGT = -999.;
  mTofMult = -999.;
  mZdcE = -999.;
  mZdcW = -999.;
  mRateZDC = -999.;
  mBbcE = -999.;
  mBbcW = -999.;
  mRateBBC = -999.;
  mVertPositionX = -999.;
  mVertPositionY = -999.;
  mVertPositionZ = -999.;
  mVpdVz = -999.;
} //EventVariablesClear

/////////////////////////////////////////////////////
void yunoPicoDstQa::TrackVariablesClear() {
  //Track counts
  iNhits = 0;
  iRCut = 0;
  iMom = 0;
  iMomPt = 0;
  iEta = 0;
  iDca = 0;

  //Track variables
  mNPrimTracks = -999.;
  mPx = -999.;
  mPy = -999.;
  mPz = -999.;
  mPt = -999.;
  mP = -999.;
  mEta = -999.;
  mPhi = -999.;
  mCharge = -999.;
  mChi2 = -999.;
  mDeDx = -999.;
  mDeDxSigmaProton = -999.;
  mDeDxSigmaKaon = -999.;
  mDeDxSigmaPion = -999.;
  mDeDxSigmaElectron = -999.;
  mDcaX = -999.;
  mDcaY = -999.;
  mDcaZ = -999.;
  mDcaMag = -999.;
  mDcaPerp = -999.;
  mNHits = -999.;
  mNHitsFit = -999.;
  mNHitsPoss = -999.;
  mNHitsDedx = -999.;
  mR = 0.0;
  mInvBeta =-999.;
  mBeta =-999.;
  mMassSqr = -999.;
  mInvBetaExpectedProton = -999.;
  mInvBetaExpectedKaon = -999.;
  mInvBetaExpectedPion = -999.;
  mInvBetaExpectedElectron = -999.;

  if(!mProtonPosId.empty())  {  mProtonPosId.clear();   }
  if(!mProtonNegId.empty())  {  mProtonNegId.clear();   }
  if(!mKaonPosId.empty())    {  mKaonPosId.clear();   }
  if(!mKaonNegId.empty())    {  mKaonNegId.clear();   }
  if(!mPionPosId.empty())    {  mPionPosId.clear();   }
  if(!mPionNegId.empty())    {  mPionNegId.clear();   }
} //TrackVariablesClear

/////////////////////////////////////////////////////
bool yunoPicoDstQa::CutRunId() {
  bool isGoodEvent = false;
  for (auto i = mBadRunId.begin(); i != mBadRunId.end(); i++) {
    if(mRunId == *i) return isGoodEvent;
  }
  isGoodEvent = true;
  return isGoodEvent;
} //CutRunId

/////////////////////////////////////////////////////
bool yunoPicoDstQa::CutTriggerId() {
  bool isGoodEvent = true;
  for (auto i = mTriggerId.begin(); i != mTriggerId.end(); i++) {
    if(mEvent->isTrigger(*i)) return isGoodEvent;
  }
  isGoodEvent = false;
  return isGoodEvent;
} //CutTriggerId

/////////////////////////////////////////////////////
bool yunoPicoDstQa::CutEvent() {
  bool isGoodEvent = false;
  if( mVertPositionZ < mVzCut[0] || mVertPositionZ > mVzCut[1]) return isGoodEvent;
  iZVertexCut++;

  if( fabs( mVertPositionX ) < mXYZCut
      && fabs( mVertPositionY ) < mXYZCut
      && fabs( mVertPositionZ ) < mXYZCut ) return isGoodEvent;
  iXYZCut++;

  if( fabs(mVpdVz - mVertPositionZ)  > mVzDiffCut ) return isGoodEvent;
  iVzDiffCut++;

  if( sqrt(pow(mVertPositionX+mShift[0], 2) + pow(mVertPositionY+mShift[1], 2)) > mXYSqrCut ) return isGoodEvent;
  iXYSqrCut++;

  isGoodEvent = true;
  return isGoodEvent;
} //CutEvent

/////////////////////////////////////////////////////
bool yunoPicoDstQa::CutTrack() {
  bool isGoodTrack = false;

  if( mNHitsCut[0] > mNHits || mNHits > mNHitsCut[1] ) return isGoodTrack;
  iNhits++;

  if(mRCut[0] > mR || mR > mRCut[1] ) return isGoodTrack;
  iRCut++;

  if(mMomCut[0] > mP || mP > mMomCut[1] ) return isGoodTrack;
  iMom++;

  if(mMomPtCut[0] > mPt || mPt > mMomPtCut[1] ) return isGoodTrack;
  iMomPt++;

  if(mEtaCut[0] > mEta || mEta > mEtaCut[1] ) return isGoodTrack;
  iEta++;

  if(mDcaCut[0] > mDcaMag || mDcaMag > mDcaCut[1] ) return isGoodTrack;
  iDca++;

  isGoodTrack = true;
  return isGoodTrack;
} //CutTrack

/////////////////////////////////////////////////////
bool yunoPicoDstQa::CutParticle(double nSigmaPart, double nSigmaOterPart1, double nSigmaOterPart2, double nSigmaOterPart3,
                                double ParticleMom,
                                double nSigmaLimit0, double nSigmaLimit1, double nSigmaLimit2, double nSigmaLimit3, double nSigmaLimit4,
                                double minMassSqr, double maxMassSqr,
                                double invDiff, double minInvBeta, double maxInvBeta) {
  if (mIdentType > 3) mIdentType = 3;

  if (mIdentType == 0) { // just TPC
    bool isGoodParticle = false;
    if (mP < ParticleMom) {
      if (fabs(nSigmaPart) <= nSigmaLimit1
          && fabs(nSigmaOterPart1) >= nSigmaLimit2
          && fabs(nSigmaOterPart2) >= nSigmaLimit3
          && fabs(nSigmaOterPart3) >= nSigmaLimit4)
        isGoodParticle = true;
    }
    return isGoodParticle;
  } //if (identType == 0) { // just TPC
  ////////////////////////////////////
  if (mIdentType == 1) { // just TOF
    bool isGoodParticle = false;
    if( mInvBeta > 0) {
      if (fabs(nSigmaPart) <= nSigmaLimit0
          && mMassSqr > minMassSqr
          && mMassSqr < maxMassSqr)
        isGoodParticle = true;
    }
    return isGoodParticle;
  } //if (identType == 1) { // just TOF
  ////////////////////////////////////
  if (mIdentType == 2) { // TPC < particleMom, TOF >= particleMom
    bool isGoodParticle = false;
    if( mInvBeta > 0
        && mP >= ParticleMom) {
      if (fabs(nSigmaPart) <= nSigmaLimit0
          && invDiff > minInvBeta
          && invDiff < maxInvBeta)
        isGoodParticle = true;
    }
    if (mP < ParticleMom) {
      if (fabs(nSigmaPart) <= nSigmaLimit1
          && fabs(nSigmaOterPart1) >= nSigmaLimit2
          && fabs(nSigmaOterPart2) >= nSigmaLimit3
          && fabs(nSigmaOterPart3) >= nSigmaLimit4)
        isGoodParticle = true;
    }
    return isGoodParticle;
  }//if (identType == 2) { // TPC < particleMom, TOF >= particleMom
  ////////////////////////////////////
  if (mIdentType == 3) { //(TPC+TOF) || TPC (pTr)
    bool isGoodParticle = false;
    if( mInvBeta > 0 && mP > ParticleMom) {
      if (fabs(nSigmaPart) <= nSigmaLimit0
          && invDiff > minInvBeta
          && invDiff < maxInvBeta)
        isGoodParticle = true;
    }
    else if( mP <= ParticleMom) {
      if (fabs(nSigmaPart) <= nSigmaLimit1
          && fabs(nSigmaOterPart1) >= nSigmaLimit2
          && fabs(nSigmaOterPart2) >= nSigmaLimit3
          && fabs(nSigmaOterPart3) >= nSigmaLimit4)
        isGoodParticle = true;
    }
    return isGoodParticle;
  } //if (identType == 3) { //TPC+TOF || TPC (pTr)
  return false; // if identType is unknown
} //CutParticle

/////////////////////////////////////////////////////
void yunoPicoDstQa::FillVtxHistogram(struct Histogram *hist) {
  vector<double> varH1D {
    mVertPositionX,
    mVertPositionY,
    mVertPositionZ,
    mVpdVz,
    (mVpdVz - mVertPositionZ),
    mRefMult,
    mTofMult,
    mZdcE,
    mZdcW,
    mRateZDC,
    mBbcE,
    mBbcW,
    mRateBBC
  };
  for (int i = 0; i < (int)(varH1D.size()); i++) {
    hist->H1D.at(i)->Fill( varH1D.at(i) );
  }
  vector<double> varH2D {
    mVertPositionX, mVertPositionY,
    mVertPositionX, mVertPositionZ,
    mVertPositionZ, mVertPositionY,
    mVertPositionZ, mVpdVz,
    mRefMult, mTofMult,
    mRefMult, mVertPositionX,
    mRefMult, mVertPositionY,
    mRefMult, mVertPositionZ,
    mRefMult, mZdcE,
    mRefMult, mZdcW,
    mRefMult, mBbcE,
    mRefMult, mBbcW,
    mZdcE, mZdcW,
    mBbcE, mBbcW
  };
  for (int i = 0; i < (int)(varH2D.size())/2; i++) {
    hist->H2D.at(i)->Fill( varH2D.at(2*i), varH2D.at(2*i + 1));
  }
} //FillVtxHistogram

/////////////////////////////////////////////////////
void yunoPicoDstQa::FillTrackHistogram(struct Histogram *hist) {
  vector<double> varH1D {
    mPx,
    mPy,
    mPz,
    mPt,
    mP,
    mCharge,
    mNHits,
    mNHitsFit,
    mNHitsPoss,
    mR,
    mEta,
    mPhi,
    mDcaX,
    mDcaY,
    mDcaZ,
    mDcaMag,
    mDcaPerp,
    mDeDx,
    mInvBeta,
    mBeta,
    mChi2,
    mNHitsDedx,
  };
  for (int i = 0; i < (int)(varH1D.size()); i++) {
    hist->H1D.at(i)->Fill( varH1D.at(i) );
  }
  vector<double> varH2D {
    mP*mCharge, mNHitsFit,
    mP*mCharge, mInvBeta,
    mP*mCharge, mMassSqr,
    mP*mCharge, mDeDx,
    mPhi, mEta,
    mP*mCharge, mDeDxSigmaPion,
    mP*mCharge, mDeDxSigmaKaon,
    mP*mCharge, mDeDxSigmaProton,
    mP*mCharge, mDeDxSigmaElectron,
    mP*mCharge, mInvBeta - mInvBetaExpectedProton,
    mP*mCharge, mInvBeta - mInvBetaExpectedKaon,
    mP*mCharge, mInvBeta - mInvBetaExpectedPion,
    mP*mCharge, mInvBeta - mInvBetaExpectedElectron,
    mPt, mEta,
    mPt, mPhi,
    mMassSqr, mDeDxSigmaPion,
    mMassSqr, mDeDxSigmaKaon,
    mMassSqr, mDeDxSigmaProton,
    mMassSqr, mDeDxSigmaElectron
  };
  for (int i = 0; i < (int)(varH2D.size())/2; i++) {
    hist->H2D.at(i)->Fill( varH2D.at(2*i), varH2D.at(2*i + 1));
  }
} //FillTrackHistogram

/////////////////////////////////////////////////////
void yunoPicoDstQa::FillPidHistogram(unsigned int iPrTr, vector<unsigned int> &PosId,
                                     vector<unsigned int> &NegId,
                                     struct Histogram *hist) {
  vector<double> varH1D {
    mP,
    mEta,
    mPhi,
    mInvBeta,
    mBeta,
    mNHitsDedx,
    mNHitsFit,
    mDeDx,
    mDcaMag,
    mDcaPerp
  };
  for (int i = 0; i < (int)(varH1D.size()); i++) {
    hist->H1D.at(i)->Fill( varH1D.at(i) );
  }
  vector<double> varH2D {
    mP*mCharge, mInvBeta,
    mP*mCharge, mMassSqr,
    mPhi, mEta,
    mP*mCharge, mDeDx,
    mP*mCharge, mInvBeta - mInvBetaExpectedProton,
    mP*mCharge, mInvBeta - mInvBetaExpectedKaon,
    mP*mCharge, mInvBeta - mInvBetaExpectedPion,
    mP*mCharge, mInvBeta - mInvBetaExpectedElectron
  };
  for (int i = 0; i < (int)(varH2D.size())/2; i++) {
    hist->H2D.at(i)->Fill( varH2D.at(2*i), varH2D.at(2*i + 1));
  }
  if(mCharge > 0) PosId.push_back(iPrTr);
  if(mCharge < 0) NegId.push_back(iPrTr);
} //SelectParticle

/////////////////////////////////////////////////////
yunoPicoDstQa::yunoPicoDstQa(){
  SetBadRunIds();
  SetTriggerIds();
  SetVzCutRange();
  SetXYZCutRange();
  SetVzDiffCutRange();
  SetXYSqrCutRange();
  SetNHitsCutRange();
  SetRCutRange();
  SetMomCutRange();
  SetMomPtCutRange();
  SetEtaCutRange();
  SetDcaCutRange();
  SetPid();
  SetProtonCutRange();
  SetKaonCutRange();
  SetPionCutRange();
}

/////////////////////////////////////////////////////
yunoPicoDstQa::~yunoPicoDstQa() {}

/////////////////////////////////////////////////////
void yunoPicoDstQa::PerformQaRegular(const char *inFile, const char *outFile){

  mOutFile = new TFile(outFile, "RECREATE");

  cout << "Creating histograms" << endl;
  CreateOutHistogram();
  cout << "Creating Vtx histograms" << endl;
  CreateVtxHistogram(&mBCVtx, "mBCVtx");
  CreateVtxHistogram(&mACVtx, "mACVtx");
  cout << "Creating Trc histograms" << endl;
  CreateTrackHistogram(&mBCtrc, "mBCtrc");
  CreateTrackHistogram(&mACtrc, "mACtrc");
  cout << "Creating pid histograms" << endl;
  CreatePidHistogram(&mProton, "mProton");
  CreatePidHistogram(&mKaon, "mKaon");
  CreatePidHistogram(&mPion, "mPion");
  cout << "creating done" << endl;

  picoReader = new StPicoDstReader(inFile);
  picoReader->Init();

  picoReader->SetStatus("*",0);
  picoReader->SetStatus("Event",1);
  picoReader->SetStatus("Track",1);
  picoReader->SetStatus("BTofHit",1);
  picoReader->SetStatus("BTofPidTraits*",1);
  if( !picoReader->chain() )  { cout << "No chain has been found." << endl; }

  EventVariablesClear();

  mNEvents = picoReader->chain()->GetEntries();
  cout << "Number of events to read: " << mNEvents << endl;

  for(unsigned int iEvent = 0; iEvent < mNEvents; iEvent++) {

    bool readEvent = picoReader->readPicoEvent(iEvent);
    if( !readEvent )
    { cout << "Can't read event." << endl; continue; }

    mDst = picoReader->picoDst();
    mEvent = mDst->event();
    if( !mEvent )
    { cout << "Can't find event." << endl; continue; }

    mNumberOfPT = mDst->numberOfTracks();
    mNumberOfGT = mEvent->numberOfGlobalTracks();
    mRunId = mEvent->runId();
    mRefMult = mEvent->refMult();
    mTofMult = mEvent->btofTrayMultiplicity();
    mVertPositionX = mEvent->primaryVertex().x();
    mVertPositionY = mEvent->primaryVertex().y();
    mVertPositionZ = mEvent->primaryVertex().z();
    mVpdVz = mEvent->vzVpd();
    mZdcE = mEvent->ZdcSumAdcEast();
    mZdcW = mEvent->ZdcSumAdcWest();
    mRateZDC = mEvent->ZDCx();
    mBbcE = mBbcW = 0; // null
    for (int i = 0; i < 24 /* hardcoded in StPicoEvent.h */; i++)
    {
      mBbcE += mEvent->bbcAdcEast(i);
      mBbcW += mEvent->bbcAdcWest(i);
    }
    mRateBBC = mEvent->BBCx();

    iBeforeAllCut++;
  
    if(!CutTriggerId()) continue;
    iTriggerId++;

    hRefMultBeforeRunIdCut->Fill(mRefMult);
    if(!CutRunId()) continue;
    iRunId++;

    if(mRefMult < 0) continue;
    iRefMultCut++;

    FillVtxHistogram(&mBCVtx);

    if(!CutEvent()) continue;

    TrackVariablesClear();
    mNPrimTracks = mDst->numberOfTracks();
    for (int iPrTr = 0; iPrTr < mDst->numberOfTracks(); iPrTr++) {

      mPrimTrack = mDst->track(iPrTr);

      if ( !mPrimTrack ) continue;
      if ( !mPrimTrack->isPrimary() ) continue;

      mPx = mPrimTrack->pMom().x();
      mPy = mPrimTrack->pMom().y();
      mPz = mPrimTrack->pMom().z();
      mPt = mPrimTrack->pPt();
      mP = mPrimTrack->pPtot();
      mEta = mPrimTrack->pMom().Eta();
      mPhi = mPrimTrack->pMom().Phi();
      mCharge = mPrimTrack->charge();
      mChi2 = mPrimTrack->chi2();
      mDeDx = mPrimTrack->dEdx();
      mDeDxSigmaProton = mPrimTrack->nSigmaProton();
      mDeDxSigmaKaon = mPrimTrack->nSigmaKaon();
      mDeDxSigmaPion = mPrimTrack->nSigmaPion();
      mDeDxSigmaElectron = mPrimTrack->nSigmaElectron();
      mDcaX = mPrimTrack->gDCAx(mEvent->primaryVertex().X());
      mDcaY = mPrimTrack->gDCAy(mEvent->primaryVertex().Y());
      mDcaZ = mPrimTrack->gDCAz(mEvent->primaryVertex().Z());
      mDcaMag = mPrimTrack->gDCA(mEvent->primaryVertex()).Mag();
      mDcaPerp = mPrimTrack->gDCAxy(mEvent->primaryVertex().X(), mEvent->primaryVertex().Y());
      mNHits = mPrimTrack->nHits();
      mNHitsFit = mPrimTrack->nHitsFit();
      mNHitsPoss = mPrimTrack->nHitsPoss();
      mNHitsDedx = mPrimTrack->nHitsDedx();
      if (mPrimTrack->nHitsPoss() != 0) {
        mR = (double)mPrimTrack->nHitsFit()/(double)mPrimTrack->nHitsPoss();
      }
      if ( mPrimTrack->isTofTrack() )
      {
        mTrait = mDst->btofPidTraits( mPrimTrack->bTofPidTraitsIndex() );
        mTOF = mTrait->btof();
        if(mTOF > 0.) {  
        mInvBeta = 1/mTrait->btofBeta();
        mBeta = mTrait->btofBeta();
        mMassSqr = mP*mP*(mInvBeta*mInvBeta - 1);
        } //mTof
      } //isTofTrack
      mInvBetaExpectedProton = sqrt(MassProton*MassProton + mP*mP)/(mP);
      mInvBetaExpectedKaon = sqrt(MassKaon*MassKaon + mP*mP)/(mP);
      mInvBetaExpectedPion = sqrt(MassPion*MassPion + mP*mP)/(mP);
      mInvBetaExpectedElectron = sqrt(MassElectron*MassElectron + mP*mP)/(mP);

      FillTrackHistogram(&mBCtrc);
      if (!CutTrack()) continue;
      FillTrackHistogram(&mACtrc);

      if (CutParticle(mDeDxSigmaProton,mDeDxSigmaPion,mDeDxSigmaKaon,mDeDxSigmaElectron,
                      mProtonMom,
                      mNSigmaProton[0],mNSigmaProton[1],mNSigmaProton[2],mNSigmaProton[3],mNSigmaProton[4],
                      mProtonMassSqr[0],mProtonMassSqr[1],
                      (mInvBeta - mInvBetaExpectedProton), mInvBetaProton[0], mInvBetaProton[1]))
      { FillPidHistogram(iPrTr, mProtonPosId, mProtonNegId,&mProton);   } //CutProton
      if (CutParticle(mDeDxSigmaKaon,mDeDxSigmaProton,mDeDxSigmaPion,mDeDxSigmaElectron,
                      mKaonMom,
                      mNSigmaKaon[0],mNSigmaKaon[1],mNSigmaKaon[2],mNSigmaKaon[3],mNSigmaKaon[4],
                      mKaonMassSqr[0],mKaonMassSqr[1],
                      (mInvBeta - mInvBetaExpectedKaon), mInvBetaKaon[0], mInvBetaKaon[1]))
      { FillPidHistogram(iPrTr, mKaonPosId, mKaonNegId, &mKaon);      } //CutKaon
      if (CutParticle(mDeDxSigmaPion,mDeDxSigmaKaon,mDeDxSigmaProton,mDeDxSigmaElectron,
                      mPionMom,
                      mNSigmaPion[0],mNSigmaPion[1],mNSigmaPion[2],mNSigmaPion[3],mNSigmaPion[4],
                      mPionMassSqr[0],mPionMassSqr[1],
                      (mInvBeta - mInvBetaExpectedPion), mInvBetaPion[0], mInvBetaPion[1]))
      { FillPidHistogram(iPrTr, mPionPosId, mPionNegId, &mPion);      } //CutPion

    } //for (int iPrTr = 0; iPrTr < mNGlobTracks; iPrTr++)

    hNtracksAfterEachCut->Fill(Form("%.0f < nHits < %.0f",mNHitsCut[0],mNHitsCut[1]), iNhits);
    hNtracksAfterEachCut->Fill(Form("%.2f < #frac{nHitsFit}{nHitsPoss} < %.2f",mRCut[0],mRCut[1]), iRCut);
    hNtracksAfterEachCut->Fill(Form("%.2f < p < %.1f",mMomCut[0],mMomCut[1]), iMom);
    hNtracksAfterEachCut->Fill(Form("%.2f < p_{T} < %.1f",mMomPtCut[0],mMomPtCut[1]), iMomPt);
    hNtracksAfterEachCut->Fill(Form("%.0f < #eta < %.0f",mEtaCut[0],mEtaCut[1]), iEta);
    hNtracksAfterEachCut->Fill(Form("%.0f < DCA < %.0f",mDcaCut[0],mDcaCut[1]), iDca);

    FillVtxHistogram(&mACVtx);

  } //for(Long64_t iEvent=0; iEvent<events2read; iEvent++)

  hNeventsAfterEachCut->Fill("Before all cuts",iBeforeAllCut);
  hNeventsAfterEachCut->Fill("After runId cut",iRunId);
  hNeventsAfterEachCut->Fill("After trigger cut",iTriggerId);
  hNeventsAfterEachCut->Fill("refMult > 0",iRefMultCut);
  hNeventsAfterEachCut->Fill(Form("%.0f < V_{z} < %.0f",mVzCut[0],mVzCut[1]),iZVertexCut);
  hNeventsAfterEachCut->Fill("|V(x&y&z)| > 1.e-5",iXYZCut);
  hNeventsAfterEachCut->Fill(Form("|vpdVz - Vz|  < %.0f",mVzDiffCut),iVzDiffCut);
  hNeventsAfterEachCut->Fill(Form("#sqrt{V_{x}^{2} + V_{y}^{2}} < %.0f",mXYSqrCut),iXYSqrCut);

  cout << "iBeforeAllCut = " << iBeforeAllCut << "\n"
       << "iRunId = " << iRunId << "\n"
       << "iTriggerId = " << iTriggerId << "\n"
       << "iRefMultCut = " << iRefMultCut << "\n"
       << "iZVertexCut = " << iZVertexCut << "\n"
       << "iXYZCut = " << iXYZCut << "\n"
       << "iVzDiffCut = " << iVzDiffCut << "\n"
       << "iXYSqrCut = " << iXYSqrCut << endl;

  cout << "Writing histograms to the output file:";
  mOutFile->Write();
  cout << "\t[DONE]" << endl;
  cout << "Closing output file:";
  mOutFile->Close();
  cout << "\t[DONE]" << endl;

  picoReader->Finish();

} // yunoPicoDstQa()

